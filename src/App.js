import React, { useState } from "react";
import axios from 'axios'
import './style.css'

const CurrencyConverterDemo = () => {
  // Could turn these 3 currency hooks into 1 hook...
  const [currency, setCurrency] = useState('CAD');
  const [currencySymbol, setCurrencySymbol] = useState('CA$') 
  const [currencyAmount, setCurrencyAmount] = useState(0)
  const [loading, setLoading] = useState(false);

  var data = {
      binance_data: {
          BTC_PRICE: 0,
          ETH_PRICE: 0,
          BNB_PRICE: 0,
          XRP_PRICE: 0,
          LTC_PRICE: 0,
          ADA_PRICE: 0,
          SOL_PRICE: 0,
          DOGE_PRICE: 0,
          XMR_PRICE: 0,
          USDT_PRICE: 0,
      },
      coingecko_data: {
          BTC_PRICE: 0,
          BTC_HIGH: 0,
          BTC_LOW: 0,
          ETH_PRICE: 0,
          ETH_HIGH: 0,
          ETH_LOW: 0,
          BNB_PRICE: 0,
          BNB_HIGH: 0,
          BNB_LOW: 0,
          XRP_PRICE: 0,
          XRP_HIGH: 0,
          XRP_LOW: 0,
          LTC_PRICE: 0,
          LTC_HIGH: 0,
          LTC_LOW: 0,
          ADA_PRICE: 0,
          ADA_HIGH: 0,
          ADA_LOW: 0,
          SOL_PRICE: 0,
          SOL_HIGH: 0,
          SOL_LOW: 0,
          DOGE_PRICE: 0,
          DOGE_HIGH: 0,
          DOGE_LOW: 0,
          XMR_PRICE: 0,
          XMR_HIGH: 0,
          XMR_LOW: 0,
          USDT_PRICE: 0,
          USDT_HIGH: 0,
          USDT_LOW: 0,
      },
  };
  const [displayData, setDisplayData] = useState(data)

  return (
    <React.Fragment>
        <h1 className="Encoder-H1"> Fiat to Crypto Currency Converter </h1>
      <div className="Encoder-Container">
        <label className="Encoder-Label" for='currency'>Amount: </label>
        <input className="Encoder-Input" type='number' id='currency' onChange={(e)=>{
          setCurrencyAmount(e.target.value);
        }}></input>
        <select className="Encoder-Select" onChange={(e) => {setCurrency(e.target.value)}}>
          <option value='CAD'>CAD</option>
          <option value='USD'>USD</option>
          <option value='EUR'>EUR</option>
          <option value='GBP'>GBP</option>
          <option value='JPY'>JPY</option>
          <option value='CNY'>CNY</option>
          <option value='KRW'>KRW</option>
          <option value='RUB'>RUB</option>
          <option value='AUD'>AUD</option>
          <option value='PLN'>PLN</option>
        </select>
      </div>

      <div className="Submit-Container">
        <button className="Submit-Button" disabled={loading} onClick={() => {
          setLoading(true);

          // Although we have the data for 24hr high and lows for cryptocurrencies with Coingecko, 
          // the current layout  doesn't have the space to display those 2 values.
          axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&ids=bitcoin%2Cethereum%2Cmonero%2Clitecoin%2Cbinance-peg-dogecoin%2Cbinance-coin-wormhole%2Cripple%2Ccardano%2Csolana%2Ctether&order=id_asc&per_page=100&page=1&sparkline=false`)
          .then((response) => {
            data.coingecko_data.BNB_PRICE = response.data[0].current_price
            data.coingecko_data.BNB_HIGH= response.data[0].high_24h;
            data.coingecko_data.BNB_LOW= response.data[0].low_24h;
            data.coingecko_data.DOGE_PRICE = response.data[1].current_price;
            data.coingecko_data.DOGE_HIGH = response.data[1].high_24h;
            data.coingecko_data.DOGE_LOW = response.data[1].low_24h;
            data.coingecko_data.BTC_PRICE = response.data[2].current_price;
            data.coingecko_data.BTC_HIGH = response.data[2].high_24h;
            data.coingecko_data.BTC_LOW = response.data[2].low_24h;
            data.coingecko_data.ADA_PRICE = response.data[3].current_price;
            data.coingecko_data.ADA_HIGH = response.data[3].high_24h;
            data.coingecko_data.ADA_LOW = response.data[3].low_24h;
            data.coingecko_data.ETH_PRICE = response.data[4].current_price;
            data.coingecko_data.ETH_HIGH = response.data[4].high_24h;
            data.coingecko_data.ETH_LOW = response.data[4].low_24h;
            data.coingecko_data.LTC_PRICE = response.data[5].current_price;
            data.coingecko_data.LTC_HIGH = response.data[5].high_24h;
            data.coingecko_data.LTC_LOW = response.data[5].low_24h;
            data.coingecko_data.XMR_PRICE = response.data[6].current_price;
            data.coingecko_data.XMR_HIGH = response.data[6].high_24h;
            data.coingecko_data.XMR_LOW = response.data[6].low_24h;
            data.coingecko_data.XRP_PRICE = response.data[7].current_price;
            data.coingecko_data.XRP_HIGH = response.data[7].high_24h;
            data.coingecko_data.XRP_LOW = response.data[7].low_24h;
            data.coingecko_data.SOL_PRICE = response.data[8].current_price;
            data.coingecko_data.SOL_HIGH = response.data[8].high_24h;
            data.coingecko_data.SOL_LOW = response.data[8].low_24h;
            data.coingecko_data.USDT_PRICE = response.data[9].current_price;
            data.coingecko_data.USDT_HIGH = response.data[9].high_24h;
            data.coingecko_data.USDT_LOW = response.data[9].low_24h;
            data.binance_data.USDT_PRICE = response.data[9].current_price;

            var price_variable = 1; 
            if(currency !== 'USD'){
                                                  // Insert API key, have to sign up for an account at FreeCurrencyAPI
              axios.get(`https://freecurrencyapi.net/api/v2/latest?apikey=YOUR-API-KEY`).then((response)=>{
                // Could do a switch here instead, but that break after each one will only make this tower taller.  
                if(currency === 'CAD') {
                    price_variable = response.data.data.CAD
                    setCurrencySymbol('CA$');
                }
                if(currency === 'GBP'){
                  price_variable = response.data.data.GBP
                  setCurrencySymbol('£');
                } 
                if(currency === 'EUR'){
                  price_variable = response.data.data.EUR
                  setCurrencySymbol('€');
                } 
                if(currency === 'JPY'){
                  price_variable = response.data.data.JPY
                  setCurrencySymbol('JP¥');
                } 
                if(currency === 'CNY'){
                  price_variable = response.data.data.CNY
                  setCurrencySymbol('CN¥');
                } 
                if(currency === 'KRW'){
                  price_variable = response.data.data.KRW
                  setCurrencySymbol('₩');
                } 
                if(currency === 'RUB'){
                  price_variable = response.data.data.RUB
                  setCurrencySymbol('₽');
                } 
                if(currency === 'AUD'){
                  price_variable = response.data.data.AUD
                  setCurrencySymbol('AU$');
                } 
                if(currency === 'PLN'){
                  price_variable = response.data.data.PLN
                  setCurrencySymbol('zł');
                } 
              })
            } else {
              setCurrencySymbol('$')
            }

            // Version 2, this should be faster. We don't need every API call to be nested, 
            // as long as there are a few nested calls at the end to change state, 
            // and thus update the page when finished fetching all data, we should be good.
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT').then((response)=>{
                data.binance_data.BTC_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT').then((response)=>{
                data.binance_data.ETH_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=BNBUSDT').then((response)=>{
              data.binance_data.BNB_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=XRPUSDT').then((response)=>{
              data.binance_data.XRP_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=LTCUSDT').then((response)=>{
              data.binance_data.LTC_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=ADAUSDT').then((response)=>{
              data.binance_data.ADA_PRICE = (response.data.price * price_variable);
            })
            axios.get('https://api.binance.com/api/v3/ticker/price?symbol=SOLUSDT').then((response)=>{
                data.binance_data.SOL_PRICE = (response.data.price * price_variable);
                axios.get('https://api.binance.com/api/v3/ticker/price?symbol=DOGEUSDT').then((response)=>{
                    data.binance_data.DOGE_PRICE = (response.data.price * price_variable);
                    axios.get('https://api.binance.com/api/v3/ticker/price?symbol=XMRUSDT').then((response)=>{
                      data.binance_data.XMR_PRICE = (response.data.price * price_variable);
                      setLoading(false);
                    })
                })
            })
        })

          return setDisplayData(data)
        }}>{loading ? 'Loading Prices...' : 'Fetch Prices'}</button>
      </div>

      <div className="Encoder-Container2">
        <h3 className="Encoder-H3"> CoinGecko API Prices</h3>
        <ul className="Encoder-UL">
          <li className="Encoder-LI">
            Bitcoin
            <img className="Encoder-Img" src='img/btc.png' alt='BTC-IMG'></img>
            1BTC: {currencySymbol}{displayData.coingecko_data.BTC_PRICE.toFixed(2)} 
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.BTC_PRICE ? (currencyAmount/displayData.coingecko_data.BTC_PRICE).toFixed(6) : 0} BTC
            </div>
          </li>
          <li className="Encoder-LI">
            Ethereum
            <img className="Encoder-Img" src='img/eth.png' alt='ETH-IMG'></img>
            1ETH: {currencySymbol}{displayData.coingecko_data.ETH_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.ETH_PRICE ? (currencyAmount/displayData.coingecko_data.ETH_PRICE).toFixed(6) : 0} ETH
            </div>
          </li>
          <li className="Encoder-LI">
            Monero
            <img className="Encoder-Img" src='img/xmr.png' alt='XMR-IMG'></img>
            1XMR: {currencySymbol}{displayData.coingecko_data.XMR_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.XMR_PRICE ? (currencyAmount/displayData.coingecko_data.XMR_PRICE).toFixed(6) : 0} XMR
            </div>
          </li>
          <li className="Encoder-LI">
            Litecoin
            <img className="Encoder-Img" src='img/ltc.png' alt='LTC-IMG'></img>
            1LTC: {currencySymbol}{displayData.coingecko_data.LTC_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.LTC_PRICE ? (currencyAmount/displayData.coingecko_data.LTC_PRICE).toFixed(6) : 0} LTC
            </div>
          </li>
          <li className="Encoder-LI">
            Dogecoin
            <img className="Encoder-Img" src='img/doge.png' alt='DOGE-IMG'></img>
            1DOGE: {currencySymbol}{displayData.coingecko_data.DOGE_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.DOGE_PRICE ? (currencyAmount/displayData.coingecko_data.DOGE_PRICE).toFixed(6) : 0} DOGE
            </div>
          </li>
          <li className="Encoder-LI">
            Binance Token
            <img className="Encoder-Img" src='img/bnb.png' alt='BNB-IMG'></img>
            1BNB: {currencySymbol}{displayData.coingecko_data.BNB_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.BNB_PRICE ? (currencyAmount/displayData.coingecko_data.BNB_PRICE).toFixed(6) : 0} BNB
            </div>
          </li>
          <li className="Encoder-LI">
            Ripple
            <img className="Encoder-Img" src='img/xrp.png' alt='XRP-IMG'></img>
            1XRP: {currencySymbol}{displayData.coingecko_data.XRP_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.XRP_PRICE ? (currencyAmount/displayData.coingecko_data.XRP_PRICE).toFixed(6) : 0} XRP
            </div>
          </li>
          <li className="Encoder-LI">
            Cardano
            <img className="Encoder-Img" src='img/ada.png' alt='ADA-IMG'></img>
            1ADA: {currencySymbol}{displayData.coingecko_data.ADA_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.ADA_PRICE ? (currencyAmount/displayData.coingecko_data.ADA_PRICE).toFixed(6) : 0} ADA
            </div>
          </li>
          <li className="Encoder-LI">
            Solana
            <img className="Encoder-Img" src='img/sol.png' alt='SOL-IMG'></img>
            1SOL: {currencySymbol}{displayData.coingecko_data.SOL_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.SOL_PRICE ? (currencyAmount/displayData.coingecko_data.SOL_PRICE).toFixed(6) : 0} SOL
            </div>
          </li>
          <li className="Encoder-LI">
            US Dollar Tether
            <img className="Encoder-Img" src='img/usdt.png' alt='USDT-IMG'></img>
            1USDT: {currencySymbol}{displayData.coingecko_data.USDT_PRICE.toFixed(2)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.coingecko_data.USDT_PRICE ? (currencyAmount/displayData.coingecko_data.USDT_PRICE).toFixed(6) : 0} USDT
            </div>
          </li>
        </ul>
        <h3 className="Encoder-H3"> Binance API Prices </h3>
        <ul className="Encoder-UL">
          <li className="Encoder-LI">
            Bitcoin
            <img className="Encoder-Img" src='img/btc.png' alt='BTC-IMG'></img>
            1BTC: {currencySymbol}{displayData.binance_data.BTC_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.BTC_PRICE ? (currencyAmount/displayData.binance_data.BTC_PRICE).toFixed(6) : 0} BTC
            </div>
          </li>
          <li className="Encoder-LI">
            Ethereum
            <img className="Encoder-Img" src='img/eth.png' alt='ETH-IMG'></img>
            1ETH: {currencySymbol}{displayData.binance_data.ETH_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.ETH_PRICE ? (currencyAmount/displayData.binance_data.ETH_PRICE).toFixed(6) : 0} ETH
            </div>
          </li>
          <li className="Encoder-LI">
            Monero
            <img className="Encoder-Img" src='img/xmr.png' alt='XMR-IMG'></img>
            1XMR: {currencySymbol}{displayData.binance_data.XMR_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.XMR_PRICE ? (currencyAmount/displayData.binance_data.XMR_PRICE).toFixed(6) : 0} XMR
            </div>
          </li>
          <li className="Encoder-LI">
            Litecoin
            <img className="Encoder-Img" src='img/ltc.png' alt='LTC-IMG'></img>
            1LTC: {currencySymbol}{displayData.binance_data.LTC_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.LTC_PRICE ? (currencyAmount/displayData.binance_data.LTC_PRICE).toFixed(6) : 0} LTC
            </div>
          </li>
          <li className="Encoder-LI">
            Dogecoin
            <img className="Encoder-Img" src='img/doge.png' alt='DOGE-IMG'></img>
            1DOGE: {currencySymbol}{displayData.binance_data.DOGE_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.DOGE_PRICE ? (currencyAmount/displayData.binance_data.DOGE_PRICE).toFixed(6) : 0} DOGE
            </div>
          </li>
          <li className="Encoder-LI">
            Binance Token
            <img className="Encoder-Img" src='img/bnb.png' alt='BNB-IMG'></img>
            1BNB: {currencySymbol}{displayData.binance_data.BNB_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.BNB_PRICE ? (currencyAmount/displayData.binance_data.BNB_PRICE).toFixed(6) : 0} BNB
            </div>
          </li>
          <li className="Encoder-LI">
            Ripple
            <img className="Encoder-Img" src='img/xrp.png' alt='XRP-IMG'></img>
            1XRP: {currencySymbol}{displayData.binance_data.XRP_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.XRP_PRICE ? (currencyAmount/displayData.binance_data.XRP_PRICE).toFixed(6) : 0} XRP
            </div>
          </li>
          <li className="Encoder-LI">
            Cardano
            <img className="Encoder-Img" src='img/ada.png' alt='ADA-IMG'></img>
            1ADA: {currencySymbol}{displayData.binance_data.ADA_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.ADA_PRICE ? (currencyAmount/displayData.binance_data.ADA_PRICE).toFixed(6) : 0} ADA
            </div>
          </li>
          <li className="Encoder-LI">
            Solana
            <img className="Encoder-Img" src='img/sol.png' alt='SOL-IMG'></img>
            1SOL: {currencySymbol}{displayData.binance_data.SOL_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.SOL_PRICE ? (currencyAmount/displayData.binance_data.SOL_PRICE).toFixed(6) : 0} SOL
            </div>
          </li>
          <li className="Encoder-LI">
            US Dollar Tether
            <img className="Encoder-Img" src='img/usdt.png' alt='USDT-IMG'></img>
            1USDT: {currencySymbol}{displayData.binance_data.USDT_PRICE.toFixed(4)}
            <div className="Encoder-CurrencyConvert">
              {currencySymbol}{currencyAmount}: 
              <br/>
              {displayData.binance_data.USDT_PRICE ? (currencyAmount/displayData.binance_data.USDT_PRICE).toFixed(6) : 0} USDT
            </div>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
}

export default CurrencyConverterDemo;

